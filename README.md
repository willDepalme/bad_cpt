# Compteur de Bad Ppoints

## Présentation

Vous souhaitez calculer la méchanceté ou la lourdeur d'un de vos amis ? Le compteur Bad Point permet de compter ces mauvais points à portée de main !

Le compteur est collaboratif, et vous pouvez suivre son avancemet en direct.

## Utilisation

Tapoter le bouton *+1* pour ajouter un point. En cas de grosse infraction ou manque de respect de la part de votre ami, rester appuyé sur le bouton pour charger un bon gros *+10*.

En cas de bonne action, vous avez la possibilité de décrémenter le compteur d'un point. Mais soyez prêt à valider les plusieurs pages de validation avant de valider l'enlèvement du point.

## Objectif

Le projet est surtout l'occasion de m'entraîner sur le framework Flutter en développant des animations de composants plus poussées et des widgets originaux (cadenas). Il me permet également de tester rapidement l'utilisation des bdd Firebase.

## Preview

<img src="screenshots/use1.gif" alt="capture" width="200"/>
<img src="screenshots/use2.gif" alt="capture" width="200"/>
<img src="screenshots/use3.gif" alt="capture" width="200"/>