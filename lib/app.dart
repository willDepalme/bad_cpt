import 'package:flutter/material.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:machu_cpt/pages/lock/lock_screen.dart';
import 'package:machu_cpt/pages/number_input/number_input_screen.dart';
import 'package:machu_cpt/pages/quick_math/quick_math_screen.dart';
import 'package:machu_cpt/widgets/special_button.dart';

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.purple,
      ),
      home: MainScreen(),
    );
  }
}


class MainScreen extends StatefulWidget {
  MainScreen({Key key}) : super(key: key);

  @override
  _MainScreenState createState() => _MainScreenState();
}

class _MainScreenState extends State<MainScreen> with TickerProviderStateMixin{

  AnimationController _controller;
  final int maxFontSize = 36;

  @override
  void initState() {
    super.initState();

    _controller = AnimationController(duration: const Duration(milliseconds: 700), vsync: this);
    Firestore.instance.collection('machu').snapshots().listen((data) {
      _controller.reset();
      _controller.forward();
    });
  }

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.deepPurple[800],
      body: Stack(
        children: <Widget>[
          StreamBuilder<QuerySnapshot>(
            stream: Firestore.instance.collection('machu').snapshots(),
            builder: (BuildContext context, AsyncSnapshot<QuerySnapshot> snapshot) {
              if (snapshot.hasError)
                return errorView(snapshot);
              switch (snapshot.connectionState) {
                case ConnectionState.waiting: 
                  return waitingView();
                default:
                  return normalView(snapshot);
              }
            },
          ),
        ],
      ) 
    );
  }

  Widget normalView(AsyncSnapshot<QuerySnapshot> snapshot) {
    int points = snapshot.data.documents.first.data["points"];
    
    return Center(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: <Widget>[
          Column(
            children: <Widget>[
              ScaleTransition(
                scale: Tween(begin: 0.75, end: 2.0)
                  .animate(CurvedAnimation(
                    parent: _controller, 
                    curve: Curves.elasticOut
                  )
                ),
                child: Text("$points", style: TextStyle(color: Colors.white, fontSize: maxFontSize - ((points.toString().length-1)*1.0)))
              ),
              SizedBox(height: 12),
              Text("Bad Points", style: TextStyle(color: Colors.white))
            ],
          ),
          SizedBox(height: 36),
          SpecialButton(
            onTap: () { addPoint(points, 1); },
            onLongPress: () { addPoint(points, 10); },
          ),
          getDecrementButton(snapshot)

        ],
      ),
    );
  }

  Widget errorView(AsyncSnapshot<QuerySnapshot> snapshot) {
    return Center(child: Text('Error: ${snapshot.error}'));
  }

  Widget waitingView() {
    return Center(
      child: Container(
        child: CircularProgressIndicator(valueColor:new AlwaysStoppedAnimation<Color>(Colors.white)),
        width: 32,
        height: 32,
      )
    );
  }

  void addPoint(int actualPoints, int pointsToAdd) {
    Firestore.instance.collection('machu').document("tT7YxIqXVKd1qjncQTnn")
      .updateData({"points": actualPoints+pointsToAdd})
      .catchError((err) {
        showDialog(context: context, child: Text("Erreur lors de l'ajout de point"));
      });
  }

  Widget getDecrementButton(AsyncSnapshot<QuerySnapshot> snapshot) {
    return FlatButton(
      child: Text("-1", style: TextStyle(color: Colors.white, fontSize: 24)),
      onPressed: () => decrement(snapshot)
    );
  }

  decrement(AsyncSnapshot<QuerySnapshot> snapshot) async{
    var continueDecrement = await showDialog(
      context: context, 
      child: AlertDialog(
        title: Text("Décrémentation du compteur"),
        content: Text("Cette action aura pour but d'enlever 1 point au compteur Bad Points. Êtes-vous sur de vouloir continuer ?"),
        actions: <Widget>[
          FlatButton(
            onPressed: () { Navigator.of(context).pop(false); }, 
            child: Text("Non")
          ),
          RaisedButton(
            onPressed: () { Navigator.of(context).pop(true); }, 
            child: Text("Oui")
          )
        ],
      )
    );

    if (continueDecrement ?? false) {
      var res = await Navigator.of(context).push(MaterialPageRoute(builder: (_) => LockScreen()));
      if (res != true) return;

      var res2 = await Navigator.of(context).push(MaterialPageRoute(builder: (_) => QuickMathScreen()));
      if (res2 != true) return;

      var res3 = await Navigator.of(context).push(MaterialPageRoute(builder: (_) => NumberInputScreen()));
      if (res3 != true) return;

      var validDecrement = await showDialog(
        context: context, 
        child: AlertDialog(                 
          title: Text("Décrémentation du compteur"),
          content: Text("Le compteur va être décrémenté d'un point"),
          actions: <Widget>[
            FlatButton(
              onPressed: () { Navigator.of(context).pop(true); }, 
              child: Text("Ok")
            ),
            RaisedButton(
              onPressed: () { Navigator.of(context).pop(false); }, 
              child: Text("Annuler")
            )
          ],
        )
      );

      if (validDecrement == true) {
        int points = snapshot.data.documents.first.data["points"];
        addPoint(points, -1);
      }
    }
  }

}