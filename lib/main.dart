import 'package:flutter/material.dart';
import 'package:machu_cpt/app.dart';
import 'package:machu_cpt/sounds/sound_service.dart';

void main() {
  WidgetsFlutterBinding.ensureInitialized();
  SoundService();
  runApp(MyApp());
}

