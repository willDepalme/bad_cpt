
import 'dart:math';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:machu_cpt/sounds/sound_service.dart';

class Lock extends StatefulWidget {

  static const _defaultColor = Colors.yellow;
  static const _defaultTextColor = Colors.black;
  static const _defaultSize = 200.0;
  static const _defaultCodeLength = 4;

  final Color lockColor;
  final Color textColor;
  final double lockSize;
  final int codeLength;
  final VoidCallback onUnlock;

  Lock({
    Key key,
    this.lockColor = _defaultColor,
    this.lockSize = _defaultSize,  
    this.codeLength = _defaultCodeLength,
    this.textColor = _defaultTextColor,
    this.onUnlock
  }) : assert(codeLength >= 3 && codeLength <= 5),
  super(key: key);

  @override
  _LockState createState() => _LockState();
}

class _LockState extends State<Lock> {

  Map<int, int> codeInput = new Map<int, int>();
  Map<int, int> codeGenerated = new Map<int, int>();

  double lockWidth;
  Color lockColor;
  Color textColor;
  int codeLength;

  bool _unlocked = false;
  int ticSoundId;

  @override
  void initState() {
    lockWidth = widget.lockSize;
    lockColor = widget.lockColor;
    codeLength = widget.codeLength;
    textColor = widget.textColor;

    initInput();
    loadSound();
    generateCode();
    super.initState();
  }

  void initInput() {
    for (int i = 0; i < codeLength; i++) {
      codeInput[i] = 0;
    }
  }

  void generateCode() {
    for (int i = 0; i < codeLength; i++) {
      codeGenerated[i] = Random().nextInt(9);
    }
    print(codeGenerated.values.map((e) => e.toString()).join(''));
  }

  void loadSound() {
    SoundService().loadSound("assets/sounds/yes.mp3");
  }

  void checkCode(int lastRowChangeIndex) {
    bool codeFound = true;
    for (int i = 0; i < codeLength; i++) {
      codeFound = codeFound && codeGenerated[i] == codeInput[i];
      if (i == lastRowChangeIndex && codeGenerated[i] == codeInput[i]) {
        playValidNumber();
      }
    }
    Future.delayed(Duration(milliseconds: 300)).then((value) {
      setState(() {
        _unlocked = codeFound;
        if (_unlocked && widget.onUnlock != null) {
          widget.onUnlock();
        }
      });
    });
  }

  void playValidNumber() async {
    print("TIC");
    SoundService().playSound();
  }
  
  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        getLocker(),
      ],
    );
  }

  @override
  void dispose() {
    SoundService().unloadSound(ticSoundId);
    super.dispose();
  }

  Widget getLocker() {

    var lockLoop =  Container(
      width: lockWidth,
      height: lockWidth * 1.5,
      child: SvgPicture.asset("assets/images/locker.svg", color: lockColor),
    );

    var lockBody = Container(
      height: lockWidth,
      width: lockWidth * 1.2,              
      padding: EdgeInsets.all(12),
      decoration: BoxDecoration(
        color: lockColor,
        borderRadius: BorderRadius.circular(8)
      ),
      child: Stack(
        children: <Widget>[
          Align(
            alignment: Alignment.center,
            child: Container(
              width: double.infinity,
              height: 2,
              color: textColor.withAlpha(100)
            ),
          ),
          getNumberRows()
        ],
      )
    );

    return Container(
      height: lockWidth * 2,
      padding: EdgeInsets.only(top: 50),
      child: Stack(
        overflow: Overflow.visible,
        alignment: AlignmentDirectional.center,
        children: <Widget>[
          AnimatedPositioned(
            top: _unlocked ? -50 : 0,
            child: lockLoop,
            duration: Duration(milliseconds: 500),
            curve: Curves.easeInOutCubic,
          ),
          Positioned(
            top: lockWidth - (lockWidth * 0.35),
            child: lockBody
          ),
        ],
      )
    );
  }

  Widget getNumberRows() {

    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      children: List.generate(codeLength, (index) => Container(
          color: lockColor,
          width: (lockWidth / codeLength) - 10,
          height: lockWidth,
          child: IgnorePointer(
            ignoring: _unlocked,
            child: CupertinoPicker(
              looping: true,
              squeeze: 2,
              backgroundColor: lockColor,
              diameterRatio: 1,
              itemExtent: lockWidth / 3, 
              children: generateNumber(10, lockWidth, index),
              onSelectedItemChanged: (int) {
                setState(() {
                  codeInput[index] = int;
                });
                Future.delayed(Duration(milliseconds: 400)).then((value) {
                  checkCode(index);
                });
              },
            )
          )
        )
      )
    );
  }

  List<Widget> generateNumber(int length, double lockWidth, int codeInputIndex) {
    return List.generate(length, (index) => Container(
        width: double.infinity,
        height: double.infinity,
        padding: EdgeInsets.all(8),
        child: Center(
          child: Text("$index", style: TextStyle(fontSize: lockWidth/10, color: textColor.withAlpha(_unlocked ? 150 : 255)))
        ),
        decoration: BoxDecoration(
          color: lockColor
        ),
      )
    );
  }

}