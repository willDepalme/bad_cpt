import 'package:flutter/material.dart';
import 'package:machu_cpt/pages/lock/lock.dart';

class LockScreen extends StatefulWidget {
  LockScreen({Key key}) : super(key: key);

  @override
  _LockScreenState createState() => _LockScreenState();
}

class _LockScreenState extends State<LockScreen> {

  bool loading = true;
  bool codeFound = false;

  @override
  void initState() {
    loadLock();
    super.initState();
  }

  void loadLock() async {
    Future.delayed(Duration(seconds: 5)).then((_) {
      setState(() {
        loading = false;
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: loading ? getLoadingContent() : getLockContent()
    );
  }

  Widget getLoadingContent() {
    return Center(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Container(
            width: 120,
            height: 120,
            child: CircularProgressIndicator(strokeWidth: 8),
          ),
          SizedBox(height: 48),
          Text("Génération du code en cours...", style: TextStyle(fontSize: 20),)
        ],
      ),
    );
  }

  Widget getLockContent() {
    return Center(
      child: Column(
        mainAxisSize: MainAxisSize.max,
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Lock(
            lockColor: Colors.grey[900],
            textColor: Colors.white,
            codeLength: 4,
            onUnlock: () {
              if (!codeFound) {
                setState(() {
                  codeFound = true;
                });
                Future.delayed(Duration(seconds: 2)).then((value) {
                  Navigator.of(context).pop(true);
                });
              }
            },
          ),
          SizedBox(height: 24),
          Text(codeFound ? "Accès autorisé" : "Trouvez le code", style: TextStyle(fontSize: 24))
        ],
      )
    );
  }
}