
import 'dart:math';

import 'package:flutter/material.dart';

class NumberInputScreen extends StatefulWidget {
  NumberInputScreen({Key key}) : super(key: key);

  @override
  _NumberInputScreenState createState() => _NumberInputScreenState();
}

class _NumberInputScreenState extends State<NumberInputScreen> {

  int number;
  String input;
  Map<int, double> numberXPositions = new Map<int, double>();
  Map<int, double> numberYPositions = new Map<int, double>();

  double headerSize = 140;
  Duration animationDuration = Duration(seconds: 1);
  bool contextReady = false;

  bool guessRight = false;
  bool guessWrong = false;
  bool disposed = false;

  @override
  void initState() {
    input = "";
    generateNumber();
    initPositions();
    super.initState();
  }

  void generateNumber() {
    setState(() {
      number = Random().nextInt(9000) + 1000;
    });
  }

  void initPositions() {
    try {
      if (context != null) {
        double width = MediaQuery.of(context).size.width;
        double height = MediaQuery.of(context).size.height - headerSize;
        setState(() {
          for (int i = 0 ; i < 10; i++) {
            numberXPositions[i] = Random().nextDouble() * width;
            numberYPositions[i] = Random().nextDouble() * height;
          }
          contextReady = true;
        });
      }
    } catch (err) {
      print(err);
    } finally {
      if (!disposed) {
        Future.delayed(animationDuration).then((value) => initPositions());
      }
    }
  }

  @override
  void dispose() {
    disposed = true;
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
       body: Column(
         children: <Widget>[
           Container(
             width: double.infinity,
             padding: EdgeInsetsDirectional.only(top: 30),
             height: headerSize,
             child: Column(
                children: [
                  Text("Entrez le code : $number", style: TextStyle(fontSize: 24),),
                  SizedBox(height: 8),
                  getUserInputDisplay(),
                ]
             ),
           ),
           getNumberInput()
         ],
       )
    );
  }

  Widget getUserInputDisplay() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      mainAxisSize: MainAxisSize.min,
      children: List.generate(number.toString().length, (index) {
        return Padding(
          padding: EdgeInsets.symmetric(horizontal: 4),
          child: Container(
          width: 40,
            height: 50,
            padding: EdgeInsets.symmetric(horizontal: 4),
            decoration: BoxDecoration(
              border: Border.all(color: getInputColor(), width: 2),
              borderRadius: BorderRadius.circular(8)
            ),
            child: Center(
              child: Text(input.length > index ? input[index] : " ", style: TextStyle(fontSize: 24),),
            ),
          )
        );
      })
    );
  }

  Widget getNumberInput() {
    return Expanded(
      child: Container(
        width: double.infinity,
        child: Stack(
          children: List.generate(10, (index) {
              return contextReady ? AnimatedPositioned(
                duration: animationDuration,
                left: numberXPositions[index],
                top: numberYPositions[index],
                child: InkWell(
                  child: Container(
                    width: 30,
                    height: 30,
                    alignment: Alignment.center,
                    child: Text(index.toString(), style: TextStyle(fontSize: 30)),
                  ),
                  onTap: () {
                    onNumberTap(index);
                  },
                )
              ) : Container();
          })
        )
      )
    );
  }

  void onNumberTap(int numberInput) {
    if (guessWrong || guessRight) return;
    setState(() {
      if (input.length < number.toString().length) {
        input += numberInput.toString();
      }
    });
    checkInput();
  }

  void checkInput() {
    if (input.length == number.toString().length) {
      if (input == number.toString()) {
        setState(() {
          guessRight = true;
        });
        Future.delayed(Duration(seconds: 2)).then((value) {
          Navigator.of(context).pop(true);
        });
      } else {
        setState(() {
          guessWrong = true;
        });
        Future.delayed(Duration(seconds: 2)).then((value) {
          setState(() {
            guessWrong = false;
            input = "";
          });
        });
      }
    }
  }

  Color getInputColor() {
    return guessRight ? Colors.green : guessWrong ? Colors.red : Colors.grey;
  }
}