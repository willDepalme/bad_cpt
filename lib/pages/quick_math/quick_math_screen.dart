
import 'dart:math';

import 'package:flutter/material.dart';

class QuickMathScreen extends StatefulWidget {
  QuickMathScreen({Key key}) : super(key: key);

  @override
  _QuickMathScreenState createState() => _QuickMathScreenState();
}

class _QuickMathScreenState extends State<QuickMathScreen> {

  final GlobalKey<FormState> _formKey = new GlobalKey<FormState>();

  int nb1, nb2;
  String operation;
  int result;
  String input;

  @override
  void initState() {
    input = "";
    generateOperation();
    super.initState();
  }

  void generateOperation() {
    setState(() {
      nb1 = Random().nextInt(99) + 1;
      nb2 = Random().nextInt(99) + 1;
      operation = Random().nextInt(2) % 2 == 0 ? '-' : '+';
      result = operation == '+' ? nb1 + nb2 : nb1 - nb2;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Column(
          mainAxisSize: MainAxisSize.min,
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Text("Entrez le résultat de l'opération", style: TextStyle(fontSize: 20)),
            SizedBox(height: 24),
            ...getContent()
          ],
        )
      ),
    );
  }

  List<Widget> getContent() {
    if (nb1 != null && nb2 != null) {
      return [
            displayOperation(),
            SizedBox(height: 24),
            displayResultForm()
          ];
    }
    return [];
  }

  Widget displayOperation() {
    return Text("$nb1 $operation $nb2 = ", style: TextStyle(fontSize: 32));
  }

  Widget displayResultForm() {
    return Container(
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          SizedBox(width: 12),
          Form(
            key: _formKey,
            child: Container(
              width: 80,
              child: TextFormField(
                textAlign: TextAlign.end,
                style: TextStyle(fontSize: 32),
                onSaved: (value) {
                  input = value;
                },
                validator: (value) {
                  var res = int.tryParse(value);
                  if (res == null) return "Non";
                  return null;
                },
              )
            )
          ),
          InkWell(
            child: Padding(
              padding: EdgeInsets.all(8),
              child: Icon(Icons.check_circle, size: 30)
            ),
            onTap: () {
              checkResultat();
            }
          )
        ],
      )
    );
  }

  void checkResultat()  {
    if (_formKey.currentState.validate()) {
      _formKey.currentState.save();
      int res = int.tryParse(input);
      if (res == result) {
        Navigator.of(context).pop(true);
      }
    }
  }


}