
import 'package:assets_audio_player/assets_audio_player.dart';

class SoundService {
  
  static final SoundService _instance = SoundService._internal();
  final assetsAudioPlayer = AssetsAudioPlayer();

  factory SoundService() {
    return _instance;
  }

  SoundService._internal() {
    print("build SoundService");
    //pool = Soundpool(streamType: StreamType.notification);
  }

  void loadSound(String url) async {
    /*return await rootBundle.load(url).then((ByteData soundData) {
      return pool.load(soundData);
    });*/
    var sound = Audio(url);
    assetsAudioPlayer.open(sound);
  }

  void unloadSound(int id) {
    //pool.release().then((value) {});
  }

  void playSound() {
    assetsAudioPlayer.play();
    //return pool.play(id);
  }

}