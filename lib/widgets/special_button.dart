import 'package:flutter/material.dart';

class SpecialButton extends StatefulWidget {

  final VoidCallback onTap;
  final VoidCallback onLongPress;

  SpecialButton({Key key, this.onTap, this.onLongPress}) : super(key: key);

  @override
  _SpecialButtonState createState() => _SpecialButtonState();
}

class _SpecialButtonState extends State<SpecialButton> with TickerProviderStateMixin {

  double normalSize = 100;
  double chargedSize = 150;
  double currentSize;
  bool isDown;
  AnimationController particuleController, particuleChargedController;

  @override
  void initState() {
    currentSize = normalSize;
    isDown = false;
    particuleController = AnimationController(duration: const Duration(milliseconds: 500), vsync: this);
    particuleController.addListener(() { setState(() {}); });
    particuleChargedController = AnimationController(duration: const Duration(milliseconds: 500), vsync: this);
    particuleChargedController.addListener(() { setState(() {}); });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    var button = GestureDetector(
      child: AnimatedContainer(
       curve: ElasticOutCurve(),
       duration: Duration(milliseconds: 500),
       height: currentSize,
       width: currentSize,
       decoration: BoxDecoration(
         color: getColor(),
         borderRadius: BorderRadius.circular(currentSize),
         border: Border.all(width:4, color: Colors.purpleAccent)
       ),
       child: Center(
         child: Text(getText(), style: TextStyle(color: Colors.white, fontWeight: FontWeight.bold, fontSize: isNormal() ? 24: 36)),
       ),
      ),
      onTap: () {
        setState(() {
          currentSize = normalSize;
        });
        particuleController.reset();
        particuleController.forward();
        widget.onTap();
      },
      onTapDown: (details) {
        setState(() {
          isDown = true;
        });
      },
      onTapUp: (details) {
        setState(() {
          isDown = false;
        });
      },
      onLongPress: () {
        setState(() {
          currentSize = chargedSize;
        });
      },
      onLongPressUp: () {
        setState(() {
          currentSize = normalSize;
        });
        particuleChargedController.reset();
        particuleChargedController.forward();
        widget.onLongPress();
      },
      onLongPressEnd: (details) {
        particuleController.reset();
      },
    );

    var particuleNormale = Positioned(
      child: Opacity(
        child: Text("+1", style: TextStyle(color: Colors.white, fontSize: 24)),
        opacity: 1 - particuleController.value,
      ),
      left: currentSize / 2,
      top: -200 * particuleController.value,
    );

    var particuleCharged = Positioned(
      child: Opacity(
        child: Text("+10", style: TextStyle(color: Colors.white, fontSize: 24)),
        opacity: 1 - particuleChargedController.value,
      ),
      left: currentSize / 2,
      top: (-200 * particuleChargedController.value) + 20,
    );

    return Stack(
      overflow: Overflow.visible,
      children: <Widget>[
        particuleNormale,
        particuleCharged,
        button
      ],
    );
  }

  String getText() {
    return isNormal() ? "+1" : "+10";
  }

  Color getColor() {
    if (isDown) {
      return isNormal() ? Colors.purple[600] : Colors.purple[300];
    } else {
      return isNormal() ? Colors.purple[500] : Colors.purple[300];
    }
  }

  bool isNormal() {
    return currentSize == normalSize;
  }
}